//Tìm số nguyên dương nhỏ nhất sao cho: 1 + 2 +...+ n>10000

function soNguyenDuongNhoNhat() {
  var sum = 0;
  var n = 0;

  while (sum < 10000) {
    n++;
    sum += n;
  }
  document.getElementById("ket_qua1").innerHTML = `${n}`;
}
